import './footer.css'
import createElement from '../utils';
import star from '../assets/star.svg';
import emptyStar from "../assets/emptyStar.svg"

const createFooter = () => {

    const footer = document.createElement('footer');

    createElement('span', footer, {
        class: "price",
        alt: "footer price",

    }, "$749" )

    
    for (let i = 0; i < 4; i++) {
     createElement('img', footer, {
        class: "star",
        src: star,
        alt: "footer star",})}

        createElement('img', footer, {
            class: "emptyStar",
            src: emptyStar,
            alt: "empty star",})


            createElement('span', footer, {
                class: "textFooter",
                alt: "footer text",
        
            }, "A Woocommerce product gallery Slider for Slider Revolution with mind-blowing visuals." )



        createElement('button', footer, {
            class: "button",
           }, "BUY NOW")








    return footer
}


export default createFooter