function createElement(tagName, parent, attributes = {}, content = '') {

         let element = document.createElement(tagName);
          Object.keys(attributes).forEach(key => {
              element.setAttribute(key, attributes[key]);
          });
          element.textContent = content;
          parent.appendChild(element);
          return element;
  }
  
  export default createElement;