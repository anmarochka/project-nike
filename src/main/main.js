import './main.css'
import arrow1 from "../assets/arrow-left.svg"
import arrow2 from "../assets/arrow-right.svg"
import circle1 from "../assets/circle.svg"
import circle2 from "../assets/circle2.svg"
import nike from "../assets/nike.svg"
import text from "../assets/text.svg"

import createElement from '../utils';



const createMain = () => {

    const main = document.createElement('main');


    const circleLeft = createElement('img', main, {
        class: "circle_left",
        src: circle1,
        alt: "circle left",


    })

    const circleRight = createElement('img', main, {
        class: "circle_right",
        src: circle2,
        alt: "circle right",

    })


    createElement('img', main, {
        class: "arrow_left",
        src: arrow1,
        alt: "arrow left",

    })

  
    
    createElement('img', main, {
        class: "arrow_right",
        src: arrow2,
        alt: "arrow right",

    })

    const shoes = createElement('img', main, {
        class: "nike_shoes",
        src: nike,
        alt: "nike shoes",
    })


    createElement('img', main, {
        class: "main_text",
        src: text,
        alt: "main text",

    })





    return main;
    
}



export default createMain